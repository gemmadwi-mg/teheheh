const express = require('express');
const router = express.Router();
const controller = require('../controllers/index')
const validate = require('../middlewares/validate')
const validation = require('../validations/usergamebiodata')


router.get('/', controller.usergamebiodata.getAll);
router.post('/', controller.auth.protect, validation.create(), validate, controller.usergamebiodata.post);
router.get('/:user_game_biodata_id', validation.findById(), validate, controller.usergamebiodata.getUserGameBiodataById);
router.put('/:user_game_biodata_id', controller.auth.protect, validation.update(), validate, controller.usergamebiodata.put);
router.delete('/:user_game_biodata_id', controller.auth.protect, validation.destroy(), validate, controller.usergamebiodata.delete);

module.exports = router;